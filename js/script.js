// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// 1. Робота з зовнішніми джерелами даних: Під час взаємодії з серверами, базами даних або іншими зовнішніми джерелами даних можуть виникнути помилки через погане підключення, неправильний формат даних тощо. 
// 2. Робота з файлами: Під час читання або запису файлів можуть виникнути помилки, такі як відсутність файлу, недостатні права доступу або неправильний шлях до файлу. 


const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70,
    },
    {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70,
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70,
    },
    {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40,
    },
    {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
    },
  ];
  
  const rootElement = document.getElementById("root");
  
  function createBookElement(book) {
    const listItem = document.createElement("li");
    listItem.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price || "Unknown"}`;
    return listItem;
  }
  
  function displayBooks() {
    const ul = document.createElement("ul");
  
    books.forEach((book) => {
      try {
        if (!("author" in book)) {
          throw new Error("Missing 'author' property");
        }
        if (!("name" in book)) {
          throw new Error("Missing 'name' property");
        }
        if (!("price" in book)) {
          throw new Error("Missing 'price' property");
        }
  
        const bookElement = createBookElement(book);
        ul.appendChild(bookElement);
      } catch (error) {
        console.error(`Invalid book: ${error.message}`);
      }
    });
  
    rootElement.appendChild(ul);
  }
  
  displayBooks();
  